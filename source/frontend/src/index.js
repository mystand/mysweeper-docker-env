import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { Route, Switch } from 'react-router'
import { ConnectedRouter } from 'connected-react-router'
import Intro from './components/pages/Intro/Intro'
import Game from './components/pages/Game/Game'
import HallOfFame from './components/pages/HallOfFame/HallOfFame'
import './index.css'
import * as serviceWorker from './serviceWorker'
import { history, store } from './store'

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <>
        <Switch>
          <Route exact path="/" render={() => (<Intro />)} />
          <Route exact path="/game" render={() => <Game />} />
          <Route exact path="/fame" render={() => <HallOfFame />} />
          <Route render={() => (<div>{"Do something with it! It's not supposed to be an Easter Egg!"}</div>)} />
        </Switch>
      </>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
