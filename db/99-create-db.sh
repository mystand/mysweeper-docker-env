#!/usr/bin/env sh

createdb mysweeper -U postgres
createdb mysweeper_test -U postgres

pg_restore -Fc -U postgres -d mysweeper /dumps/init.dump
